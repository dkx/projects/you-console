import {Gpio} from "pigpio";


// in nanoseconds
const NOISE_TIME = 200 * 1_000_000;
const MINUTE = 60 * 1_000;

/**
 * Presumes two legs
 */
export class CadenceSensor
{
	private readonly gpio: Gpio;

	private firstStep: [number, number] = [0, 0];

	private lastStep: [number, number]|null = null;

	private counter: number = 0;

	private listener:  ((level: number) => void)|null = null;

	public value: number = 0;

	constructor(gpioPin: number)
	{
		this.gpio = new Gpio(gpioPin, {
			mode: Gpio.INPUT,
			edge: Gpio.RISING_EDGE,
		});
	}

	public start(): void
	{
		if (this.listener !== null) {
			return;
		}

		this.listener = level => {
			if (level === 1) {
				this.recordStep();
			}
		};

		this.gpio.on('interrupt', this.listener);
	}

	public stop(): void
	{
		if (this.listener === null) {
			return;
		}

		this.gpio.removeListener('interrupt', this.listener);

		this.listener = null;
		this.firstStep = [0, 0];
		this.lastStep = null;
		this.counter = 0;
		this.value = 0;
	}

	private recordStep(): void
	{
		// drop noise
		if (this.lastStep !== null) {
			const elapsed = process.hrtime(this.lastStep);
			if (elapsed[0] === 0 && elapsed[1] <= NOISE_TIME) {
				this.lastStep = process.hrtime();
				return;
			}
		}

		this.lastStep = process.hrtime();

		// first foot
		if (this.counter === 0) {
			this.firstStep = this.lastStep;
		}

		// ignore second foot

		// first foot again
		if (this.counter === 2) {
			const elapsed = process.hrtime(this.firstStep);
			const nanoseconds = elapsed[0] * 1000 + elapsed[1] / 1_000_000;
			const cadence = MINUTE / nanoseconds * 2;

			this.counter = 0;
			this.value = cadence;

			return;
		}

		this.counter++;
	}
}

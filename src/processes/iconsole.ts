// must run in a separate process, because noble can not be stopped

import {DefaultDeviceConfiguration, IConsole} from "@dkx/iconsole";
import {retryPromise} from '@dkx/retry-promise';


const iConsole = new IConsole(
	new DefaultDeviceConfiguration(),
	{
		deviceRetryTimeout: 500,
		deviceResponseTimeout: 1000,
	},
);

(async () => {
	await retryPromise(() => connectAndWatch(), {
		delay: 500,
	});
})().then(() => process.exit(0)).catch(err => {
	console.error(err);
	process.exit(1);
});

async function connectAndWatch(): Promise<void>
{
	process.send!('searching');

	const device = await iConsole.findDevice(process.env.ICONSOLE_ADDRESS!);
	if (device === null) {
		throw new Error('iConsole device not found');
	}

	process.send!('connecting');
	await device.connect();
	process.send!('connected');

	await device.watchSensorsChanges(data => {
		process.send!(data);
	});

	//await device.disconnect();
}

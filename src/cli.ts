import {fork} from 'child_process';
import {Feature, Server} from "@dkx/bluetooth-rsc";
import {retryPromise} from '@dkx/retry-promise';

import {CadenceSensor} from "./cadence-sensor";
import {SensorsData} from "@dkx/iconsole";


if (typeof process.argv[2] === 'undefined') {
	throw new Error('Missing iConsole MAC address, set it with a first command argument');
}

const address = process.argv[2];
const cadence = new CadenceSensor(17);

(async () => {
	const server = await createServer();

	await retryPromise(() => createIConsoleClient(server, address), {
		delay: 500,
	});
})().then(() => process.exit(0)).catch(err => {
	console.error(err);
	process.exit(1);
});

async function createServer(): Promise<Server>
{
	const server = new Server('you-console', [
		Feature.walkingOrRunningStatus,
		Feature.totalDistance,
	]);

	process.stdout.write('Initializing server...');
	await server.init();
	console.log(' OK');

	return server;
}

function createIConsoleClient(server: Server, address: string): Promise<void>
{
	const iConsole = fork(__dirname + '/processes/iconsole.js', {
		env: {
			ICONSOLE_ADDRESS: address,
		},
	});

	iConsole.on('message', async message => {
		if (message === 'searching') {
			process.stdout.write('Searching for iConsole...');
			return;
		}

		if (message === 'connecting') {
			console.log(' OK');
			process.stdout.write('Connecting to iConsole...');
			return;
		}

		if (message === 'connected') {
			console.log(' OK');
			return;
		}

		const sensorsData = message as SensorsData;

		if (sensorsData.speed === 0) {
			cadence.stop();
		} else {
			cadence.start();
		}

		server.send({
			instantaneousSpeed: sensorsData.speed,
			instantaneousCadence: cadence.value,
			totalDistance: sensorsData.distance,
			running: true,
		});
	});

	return new Promise((resolve, reject) => {
		iConsole.on('exit', () => {
			console.log('iConsole disconnected');
			reject();
		});
	});
}
